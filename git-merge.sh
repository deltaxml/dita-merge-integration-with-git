#! /bin/bash

## See the README.md in the repo for details about how to use this.

SCRIPT_DIR="`dirname ${BASH_SOURCE[0]}`";

file1=$1
file2=$2
file3=$3

i=1;
while [ ! -z "${@: 4+$i:1}" ]
do
     if [ -n "$params" ]
     then
       params+=" "
     fi   
     params+=${@: 4+$i:1}
     i=`expr $i + 1`
done

if [ -n "$params" ]
then  
  java -jar ${SCRIPT_DIR}/../../dita-merge.jar merge concurrent3 ancestor $file1 edit1 $file2 edit2 $file3 $4 $params
else
  java -jar ${SCRIPT_DIR}/../../dita-merge.jar merge concurrent3 ancestor $file1 edit1 $file2 edit2 $file3 $4
fi

if [ ! -f ${SCRIPT_DIR}/GitCheckConflict.class ]; then
  javac ${SCRIPT_DIR}/GitCheckConflict.java
fi

backup_file_name="deltaxml_backup_"
backup_file_name+=$4

cp $4 $backup_file_name
java -cp ${SCRIPT_DIR} GitCheckConflict $4 ${SCRIPT_DIR}

if [ $? = 1 ]
then
    open $4
    conflictStatus=1;
    echo "CONFLICT (content): Merge conflict in $4"
    echo "Automatic merge failed; fix conflicts."
    while [ $conflictStatus -eq 1 ]
    do
	echo "Resolve the conflict manually in editor, save the result and then press enter to continue…"
	read
	cp $4 $backup_file_name
	java -cp ${SCRIPT_DIR} GitCheckConflict $4 ${SCRIPT_DIR}
	conflictStatus=$?;
    done
    echo Conflicts in $4 are resolved.
fi
